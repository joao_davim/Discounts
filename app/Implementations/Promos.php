<?php
namespace App\Promos;

use App\PromosInterface\PromosInterface as PromosInterface;
use Illuminate\Http\Request;
use App\DiscountTotal;
use App\DiscountXByY;
use App\DiscountCategory;
use App\Products;
use App\Customers;

class Promos implements PromosInterface {

    /*
      Function to get the parameters of the discount applied to all the list
    */
    public function GetPromoTotal(){
      try{
        $discount = DiscountTotal::where('active',1)->first();
      }catch(ModelNotFoundException $e){
        $discount = null;
      }

      return $discount;
    }


    /*
      Function to get the parameters of the discount which gives X items when buying Y items of a category
    */
    public function GetDiscountXbyY($product_category){
      try{
        $discountXbyY = DiscountXbyY::where('categoryID',$product_category)->where('active',1)->first();
      }catch(ModelNotFoundException $e){
        $discountXbyY = null;
      }

      return $discountXbyY;
    }

    /*
      Function to get the parameters of the discount applied to the cheaper item in a category when a certain number of items of that category are being bought
    */
    public function GetDiscountCategory($category_id){
      try{
        $DiscountCategory = DiscountCategory::where('category',$category_id)->where('active',1)->first();
      }catch(ModelNotFoundException $e){
        $DiscountCategory = null;
      }
      return $DiscountCategory;
    }

    /*
      Function that returns the of all the products read from the API
    */
    public function GetProductInfo(){
      $url = config('PRODUCTS_URL');
      $product_list_temp = json_decode(file_get_contents($url));

      $product_list = array();

      foreach ($product_list_temp as $individual_product) {
        $product_list[$individual_product->id] = $individual_product;
      }

      return $product_list;
    }

    /*
      Function that returns the information of a customer
    */
    public function GetCustomerInfo($customer_id){
      $url = config('CUSTOMERS_URL');
      $customers_temp = json_decode(file_get_contents($url));

      foreach ($customers_temp as $customer) {
        if($customer->id == $customer_id){
          return $customer;
        }
      }

      return null;
    }

}
