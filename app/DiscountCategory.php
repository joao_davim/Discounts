<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountCategory extends Model
{
    //
    protected $table = 'promotion_category';
}
