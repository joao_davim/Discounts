<?php
namespace App\PromosInterface;

  interface PromosInterface{
    public function GetPromoTotal();
    public function GetDiscountXbyY($product_category);
    public function GetDiscountCategory($category_id);
    public function GetProductInfo();
    public function GetCustomerInfo($customer_id);
  }
