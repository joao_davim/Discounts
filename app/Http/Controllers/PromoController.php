<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PromosInterface\PromosInterface as PromosInterface;

class PromoController extends Controller
{

  public function __construct(PromosInterface $promos)
  {
      $this->promos = $promos;
  }


  public function PromoTotal(){
    $url = config('DATA_URL');

    $json = json_decode(file_get_contents($url), true);

      $json['total'] = 0;

      $product_list = $this->promos->GetProductInfo();

      /*
        For each item in the list
      */

      foreach ($json['items'] as $item_id => $list_entry){

        $json['total'] += $json['items'][$item_id]['quantity'] * $json['items'][$item_id]['unit-price'];

        /*
          The product instance is fetched so we can know its category that is not sent in the list
        */
        $product_id = $list_entry['product-id'];
        $product = $product_list[$product_id];

        /*
          We see if there is any discount for this category
        */
        $discountXbyY = $this->promos->GetDiscountXbyY($product->category);

        if($discountXbyY){
          /*
            For this type of discount we check the number of items in the order against the number of items needed to
            get this discount
          */
          if($list_entry['quantity'] >= $discountXbyY->amount){
            /*
              If that condition is true we calculate the number of extra items the customer will carry
            */
            $extra_devices = floor($list_entry['quantity'] / $discountXbyY->amount);
            $json['items'][$item_id]['quantity'] = $list_entry['quantity'] + $extra_devices;

          }
        }

        /*
          Since we are already iterating through the array we can save the number of items per category and the cheapest one between them
        */
        if(!isset($categories[$product->category])){
          $categories[$product->category]['quantity'] = $list_entry['quantity'];
          $categories[$product->category]['cheapest'] = $list_entry['product-id'];
        }else{
          $categories[$product->category]['quantity'] += $list_entry['quantity'];
          if($list_entry['unit-price'] > $categories[$product->category]['cheapest']){
            $categories[$product->category]['cheapest'] = $list_entry['product-id'];
          }
        }
      }

      foreach ($categories as $category_id => $category) {
        /*
          We see if there is any discount for this category
        */
        $DiscountCategory = $this->promos->GetDiscountCategory($category_id);
        /*
          If there is a discount for this category
        */
        if($DiscountCategory){
          /*
            AND the quantity of items is larger than the quantity of items necessary to qualify for the discount
          */
          if($category['quantity'] > $DiscountCategory->amount){
            /*
              we get the ID of the cheapest item (from the array we constructed while iterating the shopping list)
            */
            $item_id = array_search($category['cheapest'], array_column($json['items'], 'product-id'));
            $percentage = $DiscountCategory->percentage / 100;
            /*
              We calculate the discount and update the shopping list
            */
            $json['items'][$item_id]['total'] = $json['items'][$item_id]['total'] - $percentage * $json['items'][$item_id]['unit-price'];
            $json['total'] = $json['total'] - $percentage * $json['items'][$item_id]['unit-price'];
          }
        }
      }

      /*
        We see if the discount applied to the entirety of the list is active
      */
      $discount = $this->promos->GetPromoTotal();


      /*
        before we return the updated shopping list we fetch the customer from DB to update the
        revenue we got from him/her/it
      */

      $customer = $this->promos->GetCustomerInfo($json['customer-id']);

      /*
        If the discount is active and the customer meets the threshold to the discount
        then we calculate the discount
      */
      if($discount && $customer->revenue > $discount->threshold){
        $percentage = $discount->percentage / 100;

        $json['total'] = $json['total'] - $percentage * $json['total'];
      }

      return $json;

  }
}
