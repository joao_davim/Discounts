<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Http\Controllers\PromoController;
use App\PromosInterface\PromosInterface as PromosInterface;

class discountTest extends TestCase
{
    /**
     * This function asserts that if a customer has ordered more than 1000€ he gets 10% off his order
     *
     * @return void
     */
    public function testDiscountGetPromoTotal()
    {
        /*
          To create an instance of the Controller with the function that makes all the calculations
          we first need to instance the interface
        */
        $interface_instance =  $this->app->make('App\PromosInterface\PromosInterface');

        /*
          Now we can instance the Controller
        */
        $discount_instance = new PromoController($interface_instance);

        /*
          We change the source of the order to the order we want to test
        */
        config(['DATA_URL' => 'tests\json\order1.json']);

        /*
          We run the discount function
        */
        $order = $discount_instance->PromoTotal();

        $this->assertTrue(abs($order['total'] - 44.91) < 0.0000000001);
    }

    /**
     * This function asserts that if a customer has ordered more than 1000€ he gets 10% off his order
     *
     * @return void
     */
    public function testDiscountGetPromoTotalUnder1000()
    {
        /*
          To create an instance of the Controller with the function that makes all the calculations
          we first need to instance the interface
        */
        $interface_instance =  $this->app->make('App\PromosInterface\PromosInterface');

        /*
          Now we can instance the Controller
        */
        $discount_instance = new PromoController($interface_instance);

        /*
          We change the source of the order to the order we want to test
        */
        config(['DATA_URL' => 'tests\json\order1_2.json']);

        /*
          We run the discount function
        */
        $order = $discount_instance->PromoTotal();

        $this->assertTrue(abs($order['total'] - 49.90) < 0.0000000001);
    }

    /**
     * This function asserts that if a customer has ordered five or more of products with category 2 (switches) he gets 1 extra for each 5
     *
     * @return void
     */
    public function testDiscountGetDiscountXbyY()
    {
        /*
          To create an instance of the Controller with the function that makes all the calculations
          we first need to instance the interface
        */
        $interface_instance =  $this->app->make('App\PromosInterface\PromosInterface');

        /*
          Now we can instance the Controller
        */
        $discount_instance = new PromoController($interface_instance);

        /*
          We change the source of the order to the order we want to test
          this file has an order with 5 items that to which the discount should apply
          and 10 item to which the discount should NOT apply
        */
        config(['DATA_URL' => 'tests\json\order2.json']);

        /*
          We run the discount function
        */
        $order = $discount_instance->PromoTotal();

        /*
          We assert the update in the quantity of items, but not in the cost
        */
        $this->assertTrue($order['items'][0]['quantity'] == 6 && abs($order['total'] - 24.95)<0.0000000001);
    }

    /**
     * This function asserts that if a customer has ordered five or more of products with category 2 (switches) he gets 1 extra for each 5
     *
     * @return void
     */
    public function testDiscountGetDiscountXbyYOver5Items()
    {
        /*
          To create an instance of the Controller with the function that makes all the calculations
          we first need to instance the interface
        */
        $interface_instance =  $this->app->make('App\PromosInterface\PromosInterface');

        /*
          Now we can instance the Controller
        */
        $discount_instance = new PromoController($interface_instance);

        /*
          We change the source of the order to the order we want to test
        */
        config(['DATA_URL' => 'tests\json\order2_2.json']);

        /*
          We run the discount function
        */
        $order = $discount_instance->PromoTotal();

        /*
          We assert the update in the quantity of items, but not in the cost
        */
        $this->assertTrue($order['items'][0]['quantity'] == 13 && abs($order['total'] - 54.89) < 0.0000000001);
    }

    /**
     * This function asserts that if a customer has ordered two or more products of category "Tools" (id 1), he gets a 20% discount on the cheapest product.
     *
     * @return void
     */
    public function testDiscountGetDiscountCategory()
    {
        /*
          To create an instance of the Controller with the function that makes all the calculations
          we first need to instance the interface
        */
        $interface_instance =  $this->app->make('App\PromosInterface\PromosInterface');

        /*
          Now we can instance the Controller
        */
        $discount_instance = new PromoController($interface_instance);

        /*
          We change the source of the order to the order we want to test
        */
        config(['DATA_URL' => 'tests\json\order3.json']);

        /*
          We run the discount function
        */
        $order = $discount_instance->PromoTotal();

        /*
          We assert the that the amount of items is not changed
          but the total cost of both the whole order and the entry for the cheapest item with category == 1 are updated
        */
        $this->assertTrue($order['items'][0]['quantity'] == 2 && $order['items'][1]['quantity'] == 1 && abs($order['items'][0]['total'] - 17.55) <= 0000000001  && abs($order['total'] - 67.05) <= 0000000001);
    }

    /**
     * This function asserts that if a customer has ordered two or more products of category "Tools" (id 1), he gets a 20% discount on the cheapest product.
     *
     * @return void
     */
    public function testDiscountGetDiscountCategoryLessThan2()
    {
        /*
          To create an instance of the Controller with the function that makes all the calculations
          we first need to instance the interface
        */
        $interface_instance =  $this->app->make('App\PromosInterface\PromosInterface');

        /*
          Now we can instance the Controller
        */
        $discount_instance = new PromoController($interface_instance);

        /*
          We change the source of the order to the order we want to test
        */
        config(['DATA_URL' => 'tests\json\order3_2.json']);

        /*
          We run the discount function
        */
        $order = $discount_instance->PromoTotal();

        /*
          We assert the that the amount of items is not changed
          and since he didn't order 2 or more items with category = 1 the order is unchanged
        */
        $this->assertTrue($order['items'][0]['quantity'] == 1 && abs($order['items'][0]['total'] - 9.75) <= 0000000001  && abs($order['total'] - 9.75) <= 0000000001);
    }


    /**
     * This function asserts that if a customer has ordered two or more products of category "Tools" (id 1), he gets a 20% discount on the cheapest product.
     *
     * @return void
     */
    public function testDiscountTotal()
    {
        /*
          To create an instance of the Controller with the function that makes all the calculations
          we first need to instance the interface
        */
        $interface_instance =  $this->app->make('App\PromosInterface\PromosInterface');

        /*
          Now we can instance the Controller
        */
        $discount_instance = new PromoController($interface_instance);

        /*
          We change the source of the order to the order we want to test
        */
        config(['DATA_URL' => 'tests\json\order4.json']);

        /*
          We run the discount function
        */
        $order = $discount_instance->PromoTotal();

        /*
          We assert the:
          - the total cost
          - the updated quantities of one product
          - the updated cost of other product
        */
        $this->assertTrue($order['items'][3]['quantity'] == 6 && abs($order['items'][0]['total'] - 17.55) <= 0000000001  && abs($order['total'] - 467.05) <= 0000000001);
    }
}
